function [eq,gradeq] = InverseModeSpatialRFRFR(guess,geometry,L,Kee_ext,Kee_int,qpd,wp,wd,Nf)
Nftot = 3*Nf;

basepoints = geometry.basepoints;
platpoints = geometry.platpoints;
rotparams = geometry.rotparams;

qa = guess(1:2,1);
qe = guess(1+2:2+3*Nftot,1);
qp = guess(1+2+3*Nftot:2+3*Nftot+6,1);
lambda = guess(1+2+3*Nftot+6:2+3*Nftot+6+3*6-1,1);

pplat = qp(1:3,1);

[Rp,dRpdroll,dRpdpitch,dRpdyaw] = rotationParametrization(qp(4:6,1),rotparams);


C1 = [zeros(1,5);eye(5)];
% matrix initialization
beameq = zeros(3*Nftot,1);
dbeamdqa = zeros(3*Nftot,2);
dbeamdqe = zeros(3*Nftot,3*Nftot);
dbeamdlambd = zeros(3*Nftot,6*3-1);
geomconstr = zeros(6*3-1,1);
dconsdqa = zeros(6*3-1,2);
dconsdqe = zeros(6*3-1,3*Nftot);
dconsdqp = zeros(6*3-1,6);

wrencheq = wp; % platform frame equilibrium
dwrenchdqa = zeros(6,2);
dwrenchdqe = zeros(6,3*Nftot);

dwrenchdqp = zeros(6,6);
dwrenchdlambd = zeros(6,6*3-1);

%% FIRST BEAM
% -------------------------------------------------------------
k = 1;
    % BEAM  INTEGRATION
    % extract variables
    p0 = basepoints(:,k);
    p1 = platpoints(:,k);
    pp = Rp*platpoints(:,k);
    pLp = pplat +pp;
    dppdrot = [dRpdroll*p1,dRpdpitch*p1,dRpdyaw*p1];

    h0 = eul2quat([0,0,qa(k)],'ZYX')';
    qei = qe(1+Nftot*(k-1):k*Nftot,1);
    lambdai = lambda(1+6*(k-1):6*k,1); % my mz fx fy fz in local tip frame
    wrench = lambdai;
    wrenchp = wrench; %% translate wrench as rigid body;
    
    dh0dqa = DerivativeMotorQuaternion([0,0,qa(k)]);
    dp0dqa = zeros(3,1);
    % integrate forward
    y0F = [p0;h0;dp0dqa;zeros(3*Nftot,1);dh0dqa;zeros(4*Nftot,1)];
    fun = @(s,y) OdefunAssumedForward(s,y,qei,Nf,L,'fix');
    [~,y] = ode45(fun,[0,1],y0F);
    ygeom = y(end,:)';
    
    % extract results
    hL = ygeom(4:7,1);
    Rtip = quat2rotmatrix(hL);
    pL = ygeom(1:3,1);

    [D1,D2,D3] = derivativeColRotMatQuat(hL);
    dhLdqa = ygeom(1+7+3+3*Nftot:7+3+3*Nftot+4,1);
    dhLdqe = reshape(ygeom(1+7+3+3*Nftot+4:7+3+3*Nftot+4+4*Nftot,1),4,Nftot);
    dpLdqa = ygeom(1+7:7+3,1);
    dpLdqe = reshape(ygeom(1+7+3:7+3+3*Nftot,1),3,Nftot);
    
    % integrate backward
    y0B = [wrenchp;zeros(Nftot,1);zeros(6+Nftot,1);zeros(Nftot*(6+Nftot),1);reshape(eye(6),6*6,1);zeros(6*Nftot,1)];

    % integration
    funbackward = @(s,y) OdefunAssumeBackward(s,y,qei,wd,Nf,L,'fix');
    [~,y] = ode45(funbackward,[1,0],y0B);
    yforces = y(end,:)';
    
    % extract results
    Qc =   yforces(1+6:6+Nftot,1);
    dQcdqa = yforces(1+6+Nftot+6:6+Nftot+6+Nftot,1);
    dQcdqe = reshape(yforces(1+6+Nftot+6+Nftot+6*Nftot:6+Nftot+6+Nftot+6*Nftot+Nftot*Nftot,1),Nftot,Nftot);
    dQcdw0 = reshape(yforces(1+6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*6:6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*(6+Nftot)),Nftot,6);
    
    % platform equibribrium contributions
    
    Adg = [Rtip,zeros(3); zeros(3), Rtip]; % ONLY Rotate in global frame
    gwrench = Adg*(-wrench); % the wrench is the opposite wrt the one of the beam    
    Adg2 = [eye(3),+skew(pp); zeros(3),eye(3)]; %  translate to platform origin
    
    wrencheq = wrencheq + Adg2*gwrench; % equilibrium in platform frame 
    
    matr2 = [wrench(1:3,1)'*D1;wrench(1:3,1)'*D2;wrench(1:3,1)'*D3;wrench(4:6,1)'*D1;wrench(4:6,1)'*D2;wrench(4:6,1)'*D3;];
    dgwrenchdqa = -matr2*dhLdqa;
    dgwrenchdqe = -matr2*dhLdqe;
    mat1 = [skew(dppdrot(:,1))*gwrench(4:6,1),skew(dppdrot(:,2))*gwrench(4:6,1),skew(dppdrot(:,3))*gwrench(4:6,1)];
    dwrenchdqp = dwrenchdqp +[zeros(3,3),mat1;zeros(3,6)];
    
    % equations and gradient
    beameq(1+Nftot*(k-1):Nftot*k,1) = L*Kee_ext*qei + Qc;
    dbeamdqa(1+Nftot*(k-1):Nftot*k,k) = dQcdqa;
    dbeamdqe(1+Nftot*(k-1):Nftot*k,1+Nftot*(k-1):Nftot*k) = L*Kee_ext + dQcdqe;
    dbeamdlambd(1+Nftot*(k-1):Nftot*k,1+6*(k-1):6*k) = dQcdw0;

    [angerr,dangerrdqa,dangerrdqe] = roterr(Rp,Rtip,hL,dhLdqa,dhLdqe);
    dangerrdang = [invskew(dRpdroll'*Rtip-Rtip'*dRpdroll),invskew(dRpdpitch'*Rtip-Rtip'*dRpdpitch),invskew(dRpdyaw'*Rtip-Rtip'*dRpdyaw)];
    geomconstr(1+6*(k-1):6*k,1)  = [angerr;pL-pLp;];
    dconsdqa(1+6*(k-1):6*k,k) = [dangerrdqa;dpLdqa];
    dconsdqe(1+6*(k-1):6*k,1+Nftot*(k-1):Nftot*k) = [dangerrdqe;dpLdqe];
    dconsdqp(1+6*(k-1):6*k,:) = [ zeros(3,3),-dangerrdang;-eye(3),-dppdrot;];
    dwrenchdqa(:,k) = Adg2*dgwrenchdqa;
    dwrenchdqe(:,1+Nftot*(k-1):Nftot*k) = Adg2*dgwrenchdqe;
    dwrenchdlambd(:,1+6*(k-1):6*k) = -Adg2*Adg;

    %% SECOND BEAM
% -------------------------------------------------------------
k = 2;
    % BEAM  INTEGRATION
    % extract variables
    p0 = basepoints(:,k);
    p1 = platpoints(:,k);
    pp = Rp*platpoints(:,k);
    pLp = pplat +pp;
    dppdrot = [dRpdroll*p1,dRpdpitch*p1,dRpdyaw*p1];

    h0 = eul2quat([0,0,qa(k)],'ZYX')';
    qei = qe(1+Nftot*(k-1):k*Nftot,1);
    lambdai = lambda(1+6*(k-1):6*k-1,1); % mx my mz fx fy fz in local tip frame
    wrench = (C1*lambdai);
    wrenchp = wrench; % translate wrench as rigid body
    dh0dqa = DerivativeMotorQuaternion([0,0,qa(k)]);
    dp0dqa = zeros(3,1);
    
    % integrate forward
    y0F = [p0;h0;dp0dqa;zeros(3*Nftot,1);dh0dqa;zeros(4*Nftot,1)];
    fun = @(s,y) OdefunAssumedForward(s,y,qei,Nf,L,'fix');
    [~,y] = ode45(fun,[0,1],y0F);
    ygeom = y(end,:)';
    
    % extract results
    hL = ygeom(4:7,1);
    Rtip = quat2rotmatrix(hL);
    [D1,D2,D3] = derivativeColRotMatQuat(hL);
    pL = ygeom(1:3,1);
    dhLdqa = ygeom(1+7+3+3*Nftot:7+3+3*Nftot+4,1);
    dhLdqe = reshape(ygeom(1+7+3+3*Nftot+4:7+3+3*Nftot+4+4*Nftot,1),4,Nftot);
    dpLdqa = ygeom(1+7:7+3,1);
    dpLdqe = reshape(ygeom(1+7+3:7+3+3*Nftot,1),3,Nftot);
    
    % integrate backward
    y0B = [wrenchp;zeros(Nftot,1);zeros(6+Nftot,1);zeros(Nftot*(6+Nftot),1);reshape(eye(6),6*6,1);zeros(6*Nftot,1)];

    % integration
    funbackward = @(s,y) OdefunAssumeBackward(s,y,qei,wd,Nf,L,'fix');
    [~,y] = ode45(funbackward,[1,0],y0B);
    yforces = y(end,:)';
    
    % extract results
    Qc =   yforces(1+6:6+Nftot,1);
    dQcdqa = yforces(1+6+Nftot+6:6+Nftot+6+Nftot,1);
    dQcdqe = reshape(yforces(1+6+Nftot+6+Nftot+6*Nftot:6+Nftot+6+Nftot+6*Nftot+Nftot*Nftot,1),Nftot,Nftot);
    dQcdw0 = reshape(yforces(1+6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*6:6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*(6+Nftot)),Nftot,6);
    
    % platform equibribrium contributions
    
    Adg = [Rtip,zeros(3); zeros(3), Rtip]; % ONLY Rotate in global frame
    gwrench = Adg*(-wrench); % the wrench is the opposite wrt the one of the beam    
    Adg2 = [eye(3),+skew(pp); zeros(3),eye(3)]; %  translate to platform origin
    
    wrencheq = wrencheq + Adg2*gwrench; % equilibrium in platform frame 
    
    matr2 = [wrench(1:3,1)'*D1;wrench(1:3,1)'*D2;wrench(1:3,1)'*D3;wrench(4:6,1)'*D1;wrench(4:6,1)'*D2;wrench(4:6,1)'*D3;];
    dgwrenchdqa = -matr2*dhLdqa;
    dgwrenchdqe = -matr2*dhLdqe;
    mat1 = [skew(dppdrot(:,1))*gwrench(4:6,1),skew(dppdrot(:,2))*gwrench(4:6,1),skew(dppdrot(:,3))*gwrench(4:6,1)];
    dwrenchdqp = dwrenchdqp +[zeros(3,3),mat1;zeros(3,6)];
    
    % equations and gradient
    beameq(1+Nftot*(k-1):Nftot*k,1) = L*Kee_int*qei + Qc;
    dbeamdqa(1+Nftot*(k-1):Nftot*k,k) = dQcdqa;
    dbeamdqe(1+Nftot*(k-1):Nftot*k,1+Nftot*(k-1):Nftot*k) = L*Kee_int + dQcdqe;
    dbeamdlambd(1+Nftot*(k-1):Nftot*k,1+6*(k-1):6*k-1) = dQcdw0*C1;

    [angerr,dangerrdqa,dangerrdqe] = roterr(Rp,Rtip,hL,dhLdqa,dhLdqe);
    dangerrdang = [invskew(dRpdroll'*Rtip-Rtip'*dRpdroll),invskew(dRpdpitch'*Rtip-Rtip'*dRpdpitch),invskew(dRpdyaw'*Rtip-Rtip'*dRpdyaw)];
    geomconstr(1+6*(k-1):6*k-1,1)  = [angerr(2:3,:);pL-pLp;];
    dconsdqa(1+6*(k-1):6*k-1,k) = [dangerrdqa(2:3,:);dpLdqa];
    dconsdqe(1+6*(k-1):6*k-1,1+Nftot*(k-1):Nftot*k) = [dangerrdqe(2:3,:);dpLdqe];
    dconsdqp(1+6*(k-1):6*k-1,:) = [ zeros(2,3),-dangerrdang(2:3,:);-eye(3),-dppdrot;];
    dwrenchdqa(:,k) = Adg2*dgwrenchdqa;
    dwrenchdqe(:,1+Nftot*(k-1):Nftot*k) = Adg2*dgwrenchdqe;
    dwrenchdlambd(:,1+6*(k-1):6*k-1) = -Adg2*Adg*C1;

%% THIRD BEAM
k = 3;
    % BEAM  INTEGRATION
    % extract variables
    p0 = basepoints(:,k);
    p1 = platpoints(:,k);
    pp = Rp*platpoints(:,k);
    pLp = pplat +pp;
    dppdrot = [dRpdroll*p1,dRpdpitch*p1,dRpdyaw*p1];

    h0 = eul2quat([0,0,qa(1)],'ZYX')';
    qei = qe(1+Nftot*(k-1):k*Nftot,1);
    lambdai = lambda(1+6*(k-1)-1:6*k-1,1); % my mz fx fy fz in local tip frame
    wrench = lambdai;
    wrenchp = wrench; % translate wrench as a rigid body;
    dh0dqa = DerivativeMotorQuaternion([0,0,qa(1)]);
    dp0dqa =zeros(3,1);
    
    % integrate forward
    y0F = [p0;h0;dp0dqa;zeros(3*Nftot,1);dh0dqa;zeros(4*Nftot,1)];
    fun = @(s,y) OdefunAssumedForward(s,y,qei,Nf,L,'fix');
    [~,y] = ode45(fun,[0,1],y0F);
    ygeom = y(end,:)';
    
    % extract results
    hL = ygeom(4:7,1);
    Rtip = quat2rotmatrix(hL);
    [D1,D2,D3] = derivativeColRotMatQuat(hL);
    pL = ygeom(1:3,1);
    dhLdqa = ygeom(1+7+3+3*Nftot:7+3+3*Nftot+4,1);
    dhLdqe = reshape(ygeom(1+7+3+3*Nftot+4:7+3+3*Nftot+4+4*Nftot,1),4,Nftot);
    dpLdqa = ygeom(1+7:7+3,1);
    dpLdqe = reshape(ygeom(1+7+3:7+3+3*Nftot,1),3,Nftot);
    
    % integrate backward
    y0B = [wrenchp;zeros(Nftot,1);zeros(6+Nftot,1);zeros(Nftot*(6+Nftot),1);reshape(eye(6),6*6,1);zeros(6*Nftot,1)];

    % integration
    funbackward = @(s,y) OdefunAssumeBackward(s,y,qei,wd,Nf,L,'fix');
    [~,y] = ode45(funbackward,[1,0],y0B);
    yforces = y(end,:)';
    
    % extract results
    Qc =   yforces(1+6:6+Nftot,1);
    dQcdqa = yforces(1+6+Nftot+6:6+Nftot+6+Nftot,1);
    dQcdqe = reshape(yforces(1+6+Nftot+6+Nftot+6*Nftot:6+Nftot+6+Nftot+6*Nftot+Nftot*Nftot,1),Nftot,Nftot);
    dQcdw0 = reshape(yforces(1+6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*6:6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*(6+Nftot)),Nftot,6);
    
    % platform equibribrium contributions
    
    Adg = [Rtip,zeros(3); zeros(3), Rtip]; % ONLY Rotate in global frame
    gwrench = Adg*(-wrench); % the wrench is the opposite wrt the one of the beam    
    Adg2 = [eye(3),+skew(pp); zeros(3),eye(3)]; %  translate to platform origin
    
    wrencheq = wrencheq + Adg2*gwrench; % equilibrium in platform frame 
    
    matr2 = [wrench(1:3,1)'*D1;wrench(1:3,1)'*D2;wrench(1:3,1)'*D3;wrench(4:6,1)'*D1;wrench(4:6,1)'*D2;wrench(4:6,1)'*D3;];
    dgwrenchdqa = -matr2*dhLdqa;
    dgwrenchdqe = -matr2*dhLdqe;
    mat1 = [skew(dppdrot(:,1))*gwrench(4:6,1),skew(dppdrot(:,2))*gwrench(4:6,1),skew(dppdrot(:,3))*gwrench(4:6,1)];
    dwrenchdqp = dwrenchdqp +[zeros(3,3),mat1;zeros(3,6)];
    
    % equations and gradient
    beameq(1+Nftot*(k-1):Nftot*k,1) = L*Kee_ext*qei + Qc;
    dbeamdqa(1+Nftot*(k-1):Nftot*k,1) = dQcdqa;
    dbeamdqe(1+Nftot*(k-1):Nftot*k,1+Nftot*(k-1):Nftot*k) = L*Kee_ext + dQcdqe;
    dbeamdlambd(1+Nftot*(k-1):Nftot*k,1+6*(k-1)-1:6*k-1) = dQcdw0;

    [angerr,dangerrdqa,dangerrdqe] = roterr(Rp,Rtip,hL,dhLdqa,dhLdqe);
    dangerrdang = [invskew(dRpdroll'*Rtip-Rtip'*dRpdroll),invskew(dRpdpitch'*Rtip-Rtip'*dRpdpitch),invskew(dRpdyaw'*Rtip-Rtip'*dRpdyaw)];
    geomconstr(1+6*(k-1)-1:6*k-1,1)  = [angerr;pL-pLp;];
    dconsdqa(1+6*(k-1)-1:6*k-1,1) = [dangerrdqa;dpLdqa];
    dconsdqe(1+6*(k-1)-1:6*k-1,1+Nftot*(k-1):Nftot*k) = [dangerrdqe;dpLdqe];
    dconsdqp(1+6*(k-1)-1:6*k-1,:) = [ zeros(3,3),-dangerrdang;-eye(3),-dppdrot;];
    dwrenchdqa(:,1) = dwrenchdqa(:,1) + Adg2*dgwrenchdqa;
    dwrenchdqe(:,1+Nftot*(k-1):Nftot*k) = Adg2*dgwrenchdqe;
    dwrenchdlambd(:,1+6*(k-1)-1:6*k-1) = -Adg2*Adg;
%% COLLECT
% ------------------------
% inverse problem

inv = qp(2:3)-qpd;
gradinv = [zeros(2,1),eye(2),zeros(2,3)];
% order to correct singu & stab evaluation
wrencheq = [wrencheq(4:6);wrencheq(1:3)];
dwrenchdqp = [dwrenchdqp(4:6,:);dwrenchdqp(1:3,:)];
dwrenchdlambd = [dwrenchdlambd(4:6,:);dwrenchdlambd(1:3,:)];
dwrenchdqa = [dwrenchdqa(4:6,:);dwrenchdqa(1:3,:)];
dwrenchdqe = [dwrenchdqe(4:6,:);dwrenchdqe(1:3,:)];
% collect

eq = [beameq;wrencheq;geomconstr;inv];

gradeq = [dbeamdqa,    dbeamdqe,         zeros(3*Nftot,6),  dbeamdlambd;
          dwrenchdqa,  dwrenchdqe,       dwrenchdqp,        dwrenchdlambd;
          dconsdqa,    dconsdqe,         dconsdqp,          zeros(6*3-1,3*6-1);
          zeros(2,2),  zeros(2,3*Nftot), gradinv,           zeros(2,3*6-1);];
      
end