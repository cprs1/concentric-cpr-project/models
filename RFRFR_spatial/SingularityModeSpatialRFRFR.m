function [flag1,flag2,C] = SingularityModeSpatialRFRFR(jac,Nfm,y,rotparams)

Nf = 3*Nfm;
eul = y(1+2+3*Nf+3:2+3*Nf+6,1);
D = rot2twist(eul,rotparams);
jac(1+3*Nf+3:3*Nf+6,:) = D'*jac(1+3*Nf+3:3*Nf+6,:);

A1 = jac(1:3*Nf+6,1:2);
U1 = jac(1:3*Nf+6,1+2:2+3*Nf);
P1 = jac(1:3*Nf+6,1+2+3*Nf:2+3*Nf+6);

A2 = jac(1+3*Nf+6:3*Nf+6+5*3+1,1:2);
U2 = jac(1+3*Nf+6:3*Nf+6+5*3+1,1+2:2+3*Nf);
P2 = jac(1+3*Nf+6:3*Nf+6+5*3+1,1+2+3*Nf:2+3*Nf+6);

Z = null(jac(1:3*Nf+6,1+2+3*Nf+6:2+3*Nf+6+3*5+1)');

T1 = [Z'*A1,Z'*U1, Z'*P1(:,1), Z'*P1(:,4:6);
         A2,   U2,    P2(:,1),    P2(:,4:6)];
     
T2 = [Z'*P1, Z'*U1;
         P2, U2];


flag1 = rcond(T1);
flag2 = rcond(T2);

W1 = Z'*[zeros(3*Nf,6);eye(6)];
W2 = zeros(5*3+1,6);
W = [W1;W2];
C = T2\W;


end