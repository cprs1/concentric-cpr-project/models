function flag = StabilityModeSpatialRFRFR(jac,Nfm,y,rotparams)

Nf = 3*Nfm;
eul = y(1+2+3*Nf+3:2+3*Nf+6,1);
D = rot2twist(eul,rotparams);
jac(1+3*Nf+3:3*Nf+6,:) = D'*jac(1+3*Nf+3:3*Nf+6,:);

U = jac(1:3*Nf+6,1+2:2+3*Nf);
P = jac(1:3*Nf+6,1+2+3*Nf:2+3*Nf+6);

Z = null(jac(1:3*Nf+6,1+2+3*Nf+6:2+3*Nf+6+3*5+1)');

H = [U,P];
Hr = Z'*H*Z;

[~,flag] = chol(Hr);

end