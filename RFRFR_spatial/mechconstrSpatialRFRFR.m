function flag = mechconstrSpatialRFRFR(y,th1lim,th2lim,bb,h,E,G,Nf,L,stresslim)
qA1 = wrapTo2Pi(y(1,1));
qA2 = wrapTo2Pi(y(2,1));
qe1 = y(1+2:2+3*Nf,1);
qe2 = y(1+2+3*Nf:2+2*3*Nf,1);

%% MOTOR CONSTRAINTS
flag1 = (th1lim(1)<=qA1 & qA1<=th1lim(2));      % motor constraint 1
flag2 = (th2lim(1)<=qA2 & qA2<=th2lim(2));  	% motor constraint 2

%% STRAIN CONSTRAINTS
Nsamp = 100; % sample in 100 points
s = linspace(0,L,Nsamp);
b = BaseFcnLegendre(s,L,Nf);

strain1x = b'*qe1(1+(1-1)*Nf:1*Nf,1);
strain1y = b'*qe1(1+(2-1)*Nf:2*Nf,1);
strain1z = b'*qe1(1+(3-1)*Nf:3*Nf,1);
strain2x = b'*qe2(1+(1-1)*Nf:1*Nf,1);
strain2y = b'*qe2(1+(2-1)*Nf:2*Nf,1);
strain2z = b'*qe2(1+(3-1)*Nf:3*Nf,1);
sigma1x = bb*E*strain1x/2;
sigma1y = h*E*strain1y/2;
sigma1z = sqrt(bb^2+h^2)*G*strain1z;
sigma2x = bb*E*strain2x/2;
sigma2y = h*E*strain2y/2;
sigma2z = sqrt(bb^2+h^2)*G*strain2z;

sigmaeq1 = max((sigma1x.^2+sigma1y.^2+3.*sigma1z.^2).^0.5);
sigmaeq2 = max((sigma2x.^2+sigma2y.^2+3.*sigma2z.^2).^0.5);
stress = max(sigmaeq1,sigmaeq2);
flag4 = stress<=stresslim;

%% GLOBAL FLAG
flag = (flag1 & flag2 & flag4);
end