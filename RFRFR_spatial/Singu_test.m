%% SINGU TEST
clear 
close all
clc

%%
d = 0.002;
ds = 0.008;
b1 = 0.054;
b2 = 0.040;

L = 0.470;
A = pi*d^2/4;
I = pi*d^4/64;

pA1 = [-0.032;0;0];
pA2 = [0;0;0];
pA3 = [+0.032;0;0];
basepoints = [pA1,pA2,pA3];

pB1 = [-0.032;0;0];
pB2 = [0;0;0];
pB3 = [+0.032;0;0];
platpoints = [pB1,pB2,pB3];

rotparams = 'XYZ';
geometry.basepoints = basepoints;
geometry.platpoints = platpoints;
geometry.rotparams = rotparams;
geometry.rigidprox = b1;
geometry.rigidtip = b2;

%% MATERIAL 
ns = 8;
nb = 2;
E = 36.1*10^9;
G = 14.6*10^9;
Kxe = nb*I*E;
Kye = nb*ns^2*I*E;
Kze = nb*12*ns^2*E*I*(ds)^2;
Kbt_ext = diag([Kxe;Kye;Kze]);

Kxi = 2*nb*I*E;
Kyi = 2*nb*ns^2*I*E;
Kzi = 2*nb*12*ns^2*E*I*(2*ds)^2;
Kbt_int = diag([Kxi;Kyi;Kzi]);
stresslim = Inf;

%% EXTERNAL LOADS
g = [0;0;-9.81];
rho = 1900;
f = rho*A*g;
mp = 0.218;
fext = [0;0;0]+mp*g;
wp = [zeros(3,1);fext];
wd = [zeros(3,1);f]; % distributed wrench


%% MODEL PARAMETERS
Nf = 4;
M = @(s) PhiMatr(s,1,Nf);
Kad_e = integral(@(s) M(s)'*Kbt_ext*M(s),0,1,'ArrayValued',true);
Kad_i = integral(@(s) M(s)'*Kbt_int*M(s),0,1,'ArrayValued',true);

pstart = [0;+0.55]; % position in YZ plane 

%% TRAJECTORY

% First initial guess
qa0 = [+0;-0]*pi/180;
qe0 = [-2;zeros(3*Nf-1,1);2;zeros(3*Nf-1,1);-2;zeros(3*Nf-1,1);];
qp0 = [0;pstart;zeros(3,1)];
lambda0 = zeros(3*6-1,1);
guess0 = [qa0;qe0;qp0;lambda0];
options = optimoptions('fsolve','display','iter-detailed','Maxiter',15,'SpecifyObjectiveGradient',false,'CheckGradients',false);

Npoints = 100;
T1 = NaN*zeros(Npoints,1);
T2 = NaN*zeros(Npoints,1);
stab = NaN*zeros(Npoints,1);
poss = NaN*zeros(Npoints,2);
i = 1;
flag_solve = 1;
h =[];
flag2 = 1;
while i <= Npoints && (flag_solve == 1 || flag_solve == 3)
    % solve
    fun = @(guess) InverseModeSpatialRFRFR(guess,geometry,L,Kad_e,Kad_i,pstart,wp,wd,Nf);

    [y,~,flag_solve,~,jac] = fsolve(fun,guess0,options);
    % save
    [flag1,flag2,~] = SingularityModeSpatialRFRFR(jac,Nf,y,rotparams);
    flags = StabilityModeSpatialRFRFR(jac,Nf,y,rotparams);
    poss(i,:) = pstart';
%     norm(inv(jac),Inf)
    T1(i) = flag1;
    T2(i) = flag2;
    stab(i) = flags;
    % Update
    guess0 = y;
    pstart = pstart - [0;0.01];
    
    i = i+1;

    posbeams = posSpatialRFRFR(y,geometry,Nf,L);

    qa = y(1:2,1);
    pplat = y(1+2+3*3*Nf:2+3*3*Nf+3,1);
    [Rplat,~,~,~] = rotationParametrization(y(1+2+3*3*Nf+3:2+3*3*Nf+6,1),rotparams);
    delete(h);
    h = PlotSpatialRFRFR(geometry,qa,posbeams,pplat,Rplat);
    pause(0.001);
    drawnow
    
end

%% plots
figure()
subplot(1,2,1)
semilogy(poss(:,2),T1,'LineWidth',2);
grid minor
subplot(1,2,2)
yyaxis left
semilogy(poss(:,2),T2,'LineWidth',2);
hold on
yyaxis right
semilogy(poss(:,2),stab,'LineWidth',2);
ylim([-2,50])
grid minor