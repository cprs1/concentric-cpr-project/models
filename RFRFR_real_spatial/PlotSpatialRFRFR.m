function h = PlotSpatialRFRFR(geometry,qa,posbeams,pplat,Rplat)
basepoints = geometry.basepoints;
platpoints = geometry.platpoints;


% figure()
% plot fixed frames
yvect = [0;1;0]/5;
xvect = [1;0;0]/5;
zvect = [0;0;1]/5;
quiver3(0,0,0,xvect(1),xvect(2),xvect(3),'Color','r','LineWidth',1.5)
hold on
quiver3(0,0,0,yvect(1),yvect(2),yvect(3),'Color','b','LineWidth',1.5)
quiver3(0,0,0,zvect(1),zvect(2),zvect(3),'Color','k','LineWidth',1.5)

h = [];
for i = 1:2
   plot3(basepoints(1,i),basepoints(2,i),basepoints(3,i),'bo','LineWidth',1.5)
   xlocal = Rx(qa(i,1))*xvect;
   ylocal = Rx(qa(i,1))*yvect;
   zlocal = Rx(qa(i,1))*zvect;
   pg = pplat + Rplat*platpoints(:,i);
   h4 = line([basepoints(1,i),posbeams(1+3*(i-1),1)],[basepoints(2,i),posbeams(1+1+3*(i-1),1)],[basepoints(3,i),posbeams(1+1+1+3*(i-1),1)],'Color','red','LineWidth',1.5);
   h3 = line([pg(1),posbeams(1+3*(i-1),end)],[pg(2),posbeams(1+1+3*(i-1),end)],[pg(3),posbeams(1+1+1+3*(i-1),end)],'Color','red','LineWidth',1.5);
   h2 = line([pg(1),pplat(1)],[pg(2),pplat(2)],[pg(3),pplat(3)],'Color','red','LineWidth',1.5);

   yi = posbeams(1+3*(i-1):3*i,:);
   h5 = plot3(yi(1,:),yi(2,:),yi(3,:),'k','LineWidth',1.5);
   h = [h;h2;h3;h4;h5];
end

% i = 3;
plot3(basepoints(1,3),basepoints(2,3),basepoints(3,3),'bo','LineWidth',1.5)
   xlocal = Rx(qa(1,1))*xvect;
   ylocal = Rx(qa(1,1))*yvect;
   zlocal = Rx(qa(1,1))*zvect;
   yi = posbeams(1+3*(3-1):3*3,:);
   pg = pplat + Rplat*platpoints(:,3);

   h3 = line([pg(1),posbeams(1+3*(3-1),end)],[pg(2),posbeams(1+1+3*(3-1),end)],[pg(3),posbeams(1+1+1+3*(3-1),end)],'Color','red','LineWidth',1.5);
   h2 = line([pg(1),pplat(1)],[pg(2),pplat(2)],[pg(3),pplat(3)],'Color','red','LineWidth',1.5);

   h4 = line([basepoints(1,3),posbeams(1+3*(3-1),1)],[basepoints(2,3),posbeams(1+1+3*(3-1),1)],[basepoints(3,3),posbeams(1+1+1+3*(3-1),1)],'Color','red','LineWidth',1.5);
   h5 = plot3(yi(1,:),yi(2,:),yi(3,:),'k','LineWidth',1.5);
   h = [h;h3;h2;h4;h5];
   



axis equal
axis([-.6 .6 -.6 .6 -.6 .6])
grid on

xlabel('x')
ylabel('y')
zlabel('z')
end