%% GEOMETRY
clear
close all
clc

%%
d = 0.002;
ds = 0.008;
b1 = 0.054;
b2 = 0.040;

L = 0.470;
A = pi*d^2/4;
I = pi*d^4/64;

pA1 = [-0.032;0;0];
pA2 = [0;0;0];
pA3 = [+0.032;0;0];
basepoints = [pA1,pA2,pA3];

pB1 = [-0.032;0;0];
pB2 = [0;0;0];
pB3 = [+0.032;0;0];
platpoints = [pB1,pB2,pB3];

rotparams = 'XYZ';
geometry.basepoints = basepoints;
geometry.platpoints = platpoints;
geometry.rotparams = rotparams;
geometry.rigidprox = b1;
geometry.rigidtip = b2;

%% MATERIAL 
ns = 8;
nb = 2;
E = 36.1*10^9;
G = 14.6*10^9;
Kxe = nb*I*E;
Kye = nb*ns^2*I*E;
Kze = nb*12*ns^2*E*I*(ds)^2;
Kbt_ext = diag([Kxe;Kye;Kze]);

Kxi = 2*nb*I*E;
Kyi = 2*nb*ns^2*I*E;
Kzi = 2*nb*12*ns^2*E*I*(2*ds)^2;
Kbt_int = diag([Kxi;Kyi;Kzi]);
stresslim = Inf;

%% EXTERNAL LOADS
g = [0;0;-9.81];
rho = 1900;
f = rho*A*g;
mp = 0.218;
fext = [0;0;0]+mp*g;
wp = [zeros(3,1);fext];
wd = [zeros(3,1);f]; % distributed wrench


%% MODEL PARAMETERS
Nf = 6;
M = @(s) PhiMatr(s,1,Nf);
Kad_e = integral(@(s) M(s)'*Kbt_ext*M(s),0,1,'ArrayValued',true);
Kad_i = integral(@(s) M(s)'*Kbt_int*M(s),0,1,'ArrayValued',true);

th1lim = [0,2*pi];
th2lim = [0,2*pi];

pstart = [0;+0.4]; % position in YZ plane 
params.pstart = pstart;

%% Simulation functions
fcn.objetivefcn = @(y,pend) InverseModeSpatialRFRFR(y,geometry,L,Kad_e,Kad_i,pend,wp,wd,Nf);
fcn.mechconstrfcn = @(y) mechconstrSpatialRFRFR(y,th1lim,th2lim,d/2,d/2,E,G,Nf,L,stresslim);
fcn.singufcn = @(jac,y) SingularityModeSpatialRFRFR(jac,Nf,y,rotparams);
fcn.stabilityfcn = @(jac,y) StabilityModeSpatialRFRFR(jac,Nf,y,rotparams);
fcn.internfcn = @(y) InternalEnergy(y,Kad_e,L,Nf);

%% First initial guess
qa0 = [+0;-0]*pi/180;
qe0 = [-2;zeros(3*Nf-1,1);2;zeros(3*Nf-1,1);-2;zeros(3*Nf-1,1);];
qp0 = [0;pstart;zeros(3,1)];
lambda0 = zeros(3*6-1,1);
guess0 = [qa0;qe0;qp0;lambda0];
options = optimoptions('fsolve','display','iter-detailed','SpecifyObjectiveGradient',true,'CheckGradients',true);

% fun = @(guess) InverseModeSpatialRFRFR(guess,geometry,L,Kad_e,Kad_i,pstart,wp,wd,Nf);
fun = @(guess) ForwardModeSpatialRFRFR(guess,geometry,L,Kad_e,Kad_i,[-pi/3;pi/3],wp,wd,Nf);

[sol,~,flag,~,jac] = fsolve(fun,guess0,options);

params.y0 = sol;

posbeams = posSpatialRFRFR(sol,geometry,Nf,L);

qa = sol(1:2,1);
pplat = sol(1+2+3*3*Nf:2+3*3*Nf+3,1);
[Rplat,~,~,~] = rotationParametrization(sol(1+2+3*3*Nf+3:2+3*3*Nf+6,1),rotparams);

PlotSpatialRFRFR(geometry,qa,posbeams,pplat,Rplat);
