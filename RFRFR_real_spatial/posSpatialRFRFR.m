function posbeams = posSpatialRFRFR(guess,geometry,Nf,L)
Nsh = 100;
basepoints = geometry.basepoints;
b1 = geometry.rigidprox;
qa = guess(1:2,1);
qe = guess(1+2:2+3*3*Nf,1);

posbeams = zeros(3*3,Nsh);

%% BEAM 1
    % extract variables
    p01 = basepoints(:,1) + Rx(qa(1))*[0;0;b1];
    h01 = eul2quat([0,0,qa(1)],'ZYX')';
    qe1 = qe(1+3*Nf*(1-1):1*3*Nf,1);
    % integrate
    y01 = [p01;h01];
    fun1 = @(s,y) OdeFunReconstruct(s,y,qe1,Nf,L);
    [s1,y1] = ode45(fun1,[0,1],y01);

    % spline results
    sspan1 = linspace(0,L,Nsh);
    posbeams(1+3*(1-1):3*1,:) = spline(L*s1,y1(:,1:3)',sspan1);
    
    p03 = basepoints(:,3) + Rx(qa(1))*[0;0;b1];
    h03 = h01;
    qe3 = qe(1+3*Nf*(3-1):3*3*Nf,1);
    % integrate
    y03 = [p03;h03];
    fun3 = @(s,y) OdeFunReconstruct(s,y,qe3,Nf,L);
    [s3,y3] = ode45(fun3,[0,1],y03);

    % spline results
    sspan3 = linspace(0,L,Nsh);
    posbeams(1+3*(3-1):3*3,:) = spline(L*s3,y3(:,1:3)',sspan3);

%% BEAM 2
% extract variables
    p02 = basepoints(:,2) + Rx(qa(2))*[0;0;b1];
    h02 = eul2quat([0,0,qa(2)],'ZYX')';
    qe2 = qe(1+3*Nf*(2-1):2*3*Nf,1);
    % integrate
    y02 = [p02;h02];
    fun2 = @(s,y) OdeFunReconstruct(s,y,qe2,Nf,L);
    [s2,y2] = ode45(fun2,[0,1],y02);

    % spline results
    sspan2 = linspace(0,L,Nsh);
    posbeams(1+3*(2-1):3*2,:) = spline(L*s2,y2(:,1:3)',sspan2);

end