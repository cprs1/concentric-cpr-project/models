function qd = DerivativeMotorQuaternion(eul)
%% CASE ZYX
% three rotations about mobile axis: the third is actuated
c = cos(eul/2);
s = sin(eul/2);
qd = 0.5*[-c(:,1).*c(:,2).*s(:,3)+s(:,1).*s(:,2).*c(:,3);
      +c(:,1).*c(:,2).*c(:,3)+s(:,1).*s(:,2).*s(:,3);
      -c(:,1).*s(:,2).*s(:,3)+s(:,1).*c(:,2).*c(:,3);
      -s(:,1).*c(:,2).*s(:,3)-c(:,1).*s(:,2).*c(:,3)];
        
end