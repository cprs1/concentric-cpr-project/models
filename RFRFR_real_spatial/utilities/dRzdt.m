% DERIVATIVE OF ROTATION MATRIX Z WRT ITS ANGLE
function R = dRzdt(t)
R = [-sin(t),-cos(t),0;
      cos(t),-sin(t),0;
      0,0,0];

end