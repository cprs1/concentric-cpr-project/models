function [err,derrdqa,derrdqe] = roterr(p,t,h,dhdqa,dhdqe)

errx = p(1,2)*t(1,3)+p(2,2)*t(2,3)+p(3,2)*t(3,3)-p(1,3)*t(1,2)-p(2,3)*t(2,2)-p(3,3)*t(3,2);
erry = p(1,3)*t(1,1)+p(2,3)*t(2,1)+p(3,3)*t(3,1)-p(1,1)*t(1,3)-p(2,1)*t(2,3)-p(3,1)*t(3,3);
errz = p(1,1)*t(1,2)+p(2,1)*t(2,2)+p(3,1)*t(3,2)-p(1,2)*t(1,1)-p(2,2)*t(2,1)-p(3,2)*t(3,1);

err = [errx;erry;errz];

derrdqa = [];
derrdqe = [];

if nargin>2
    [D1,D2,D3] = derivativeColRotMatQuat(h);
    dd1dqa = D1*dhdqa;
    dd2dqa = D2*dhdqa;
    dd3dqa = D3*dhdqa;
    dd1dqe = D1*dhdqe;
    dd2dqe = D2*dhdqe;
    dd3dqe = D3*dhdqe;
    derrxdqa = p(1,2)*dd1dqa(3,:)+p(2,2)*dd2dqa(3,:)+p(3,2)*dd3dqa(3,:)-p(1,3)*dd1dqa(2,:)-p(2,3)*dd2dqa(2,:)-p(3,3)*dd3dqa(2,:);
    derrxdqe = p(1,2)*dd1dqe(3,:)+p(2,2)*dd2dqe(3,:)+p(3,2)*dd3dqe(3,:)-p(1,3)*dd1dqe(2,:)-p(2,3)*dd2dqe(2,:)-p(3,3)*dd3dqe(2,:);
    derrydqa = p(1,3)*dd1dqa(1,:)+p(2,3)*dd2dqa(1,:)+p(3,3)*dd3dqa(1,:)-p(1,1)*dd1dqa(3,:)-p(2,1)*dd2dqa(3,:)-p(3,1)*dd3dqa(3,:);
    derrydqe = p(1,3)*dd1dqe(1,:)+p(2,3)*dd2dqe(1,:)+p(3,3)*dd3dqe(1,:)-p(1,1)*dd1dqe(3,:)-p(2,1)*dd2dqe(3,:)-p(3,1)*dd3dqe(3,:);
    derrzdqa = p(1,1)*dd1dqa(2,:)+p(2,1)*dd2dqa(2,:)+p(3,1)*dd3dqa(2,:)-p(1,2)*dd1dqa(1,:)-p(2,2)*dd2dqa(1,:)-p(3,2)*dd3dqa(1,:);
    derrzdqe = p(1,1)*dd1dqe(2,:)+p(2,1)*dd2dqe(2,:)+p(3,1)*dd3dqe(2,:)-p(1,2)*dd1dqe(1,:)-p(2,2)*dd2dqe(1,:)-p(3,2)*dd3dqe(1,:);
    derrdqa = [derrxdqa;derrydqa;derrzdqa];
    derrdqe = [derrxdqe;derrydqe;derrzdqe];
end