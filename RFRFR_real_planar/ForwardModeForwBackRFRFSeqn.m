
function [eq,gradeq] = ForwardModeForwBackRFRFSeqn(guess,geometry,Kee,L,wp,wd,qad,Nf)

basepoints = geometry.basepoints;
b1 = geometry.rigidprox;
b2 = geometry.rigidtip;

Ci = [zeros(1,2);eye(2)];


beameq = zeros(2*Nf,1);
closureeq = zeros(2*2,1); 
dbeameqdqa = zeros(2*Nf,2);
dbeameqdqe = zeros(2*Nf,2*Nf);
dbeamdeqdlamb = zeros(2*Nf,2*2);
dwrenchdqa = zeros(2,2);
dwrenchdqe = zeros(2,2*Nf);
dwrenchdlamb = zeros(2,2*2);
dclosurdqa = zeros(2*2,2);
dclosurdqe = zeros(2*2,2*Nf);
dclosurdqp = zeros(2*2,2);

qa = guess(1:2,1);
qe = guess(1+2:2+2*Nf,1);
qp = guess(1+2+2*Nf:2+2*Nf+2,1);
lambda = guess(1+2+2*Nf+2:2+2*Nf+2+2*2,1);

pplat = qp;

wrencheq = wp;

for i = 1:2
    % extract variables
    qai = qa(i);
    qei = qe(1+Nf*(i-1):Nf*i,1);
    lambdai = lambda(1+2*(i-1):2*i,1);
    p0 = basepoints(:,i) + Rz(qai)*[b1;0];
    th0 = qai;
    
    %% FORWARD INTEGRATION: Geometry
    % initial value at s = 0
    dp0dqa = Rz(qai+pi/2)*[b1;0];
    
    y01 = [p0;th0;dp0dqa;zeros(2*Nf,1);1;zeros(Nf,1)];
    
    % integration
    funforward = @(s,y) OdefunAssumedForward(s,y,qei,Nf,L,'fix');
    [~,y] = ode45(funforward,[0,1],y01);
    ygeom = y(end,:)';

    % extract results
    pL = ygeom(1:2,1);
    thL = ygeom(3,1);
    dpdqaL = ygeom(4:5,1);
    dpxdqeL = ygeom(1+5:5+Nf,1);
    dpydqeL = ygeom(1+5+Nf:5+2*Nf,1);
    dpdqeL = [dpxdqeL';dpydqeL'];
    dthdqaL = ygeom(1+5+2*Nf,1); 
    dthdqeL = ygeom(1+5+2*Nf+1:end,1)';     
    
    % wrench on local - tip frame. 
    wrenchp = (Ci*lambdai); % platform wrench on tip frame
    trans = [b2;0]; % transport vector on tip frame
    transwrench = -trans(2)*lambdai(1)+trans(1)*lambdai(2); % transport wrench
    wrench = wrenchp + [transwrench;zeros(2,1)]; % wrench tip frame local!
    dw0dlambd = Ci + [-trans(2),+trans(1);zeros(2,2)]; 
    %% BACKWARD INTEGRATION: Loads
    % initial values at s = L
  
    y02 = [thL;wrench;zeros(Nf,1);
           dthdqaL;zeros(3+Nf,1);
           dthdqeL';zeros((3+Nf)*Nf,1);
          zeros(3,1);reshape(eye(3),3*3,1);zeros(3*Nf,1);
          1;zeros(3+Nf,1)];

            
    % integration
    funbackward = @(s,y) OdefunAssumeBackward(s,y,qei,wd,Nf,L,'fix');
    [~,y] = ode45(funbackward,[1,0],y02);
    yforces = y(end,:)';
    
    % extract results
    Qc =   yforces(1+4:4+Nf,1); 
    dQcdqai = yforces(1+4+Nf+4:4+Nf+4+Nf,1);
    dQcdqei = reshape(yforces(1+4+Nf+4+Nf+4*Nf:4+Nf+4+Nf+4*Nf+Nf*Nf,1),Nf,Nf);
    dQcdw0 = reshape(yforces(1+4+Nf+4+Nf+4*Nf+Nf*Nf+4*3:4+Nf+4+Nf+4*Nf+Nf*Nf+4*3+3*Nf,1),Nf,3);
    dQcdlamb = dQcdw0 * dw0dlambd;
     
    %% EQUATIONS
    % beam equations and gradient components IN LOCAL FRAME
    beameq(1+Nf*(i-1):Nf*i,1) = L*Kee*qei+Qc;
    dbeameqdqa(1+Nf*(i-1):Nf*i,i) = dQcdqai;
    dbeameqdqe(1+Nf*(i-1):Nf*i,1+Nf*(i-1):Nf*i) = L*Kee + dQcdqei;
    dbeamdeqdlamb(1+Nf*(i-1):Nf*i,1+2*(i-1):2*i) = dQcdlamb;
        
    %------------------------------------------
    % contribution to equilibrium and gradient components in GLOBAL FRAME
    wrencheq = wrencheq - Rz(thL)*wrenchp(2:3);
    dwrenchdqa(:,i) = - Rz(thL+pi/2)*wrenchp(2:3)*dthdqaL;
    dwrenchdqe(:,1+Nf*(i-1):Nf*i) = - Rz(thL+pi/2)*wrenchp(2:3)*dthdqeL;
    dwrenchdlamb(:,1+2*(i-1):2*i) = - Rz(thL);
    
     %------------------------------------------
    % closure loop equations and gradient components + wrench eqs
    closureeq(1+2*(i-1):2*i,1) = (pL+Rz(thL)*trans)-pplat;
    dclosurdqa(1+2*(i-1):2*i,i) = dpdqaL + Rz(thL+pi/2)*[b2;0]*dthdqaL;
    dclosurdqe(1+2*(i-1):2*i,1+Nf*(i-1):Nf*i) = dpdqeL + Rz(thL+pi/2)*[b2;0]*dthdqeL;
    dclosurdqp(1+2*(i-1):2*i,:) = -eye(2);
    
end

%---------------------------------------
% forward problem equations
foreq = qa-qad;

%----------------------------------------
% equations and gradient wrt variables
eq = [beameq;wrencheq;closureeq;foreq;];
gradeq = [dbeameqdqa, dbeameqdqe, zeros(2*Nf,2), dbeamdeqdlamb;
          dwrenchdqa, dwrenchdqe, zeros(2,2), dwrenchdlamb;
          dclosurdqa, dclosurdqe, dclosurdqp, zeros(2*2,2*2);
          eye(2,2), zeros(2,2*Nf), zeros(2), zeros(2,2*2);];

end  