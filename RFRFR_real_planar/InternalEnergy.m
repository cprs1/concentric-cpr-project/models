function [energy,epsil,torque] = InternalEnergy(y,Kad,L,Nf,r,geometry,wd)
qe = y(1+2:2+2*Nf,1);
qe1 = qe(1+Nf*(1-1):Nf*1,1);
qe2 = qe(1+Nf*(2-1):Nf*2,1);
energy = 0.5*L*qe1'*Kad*qe1 + 0.5*L*qe2'*Kad*qe2;

s = linspace(0,L,100);
b = BaseFcnLegendre(s,L,Nf);
strain1 = r*b'*qe1;
strain2 = r*b'*qe2;
epsil = max(abs([strain1;strain2]));

[F1,F2] = getBaseEfforts(y,geometry,L,wd,Nf);
torque = max(abs(F1(1)),abs(F2(1)));
end