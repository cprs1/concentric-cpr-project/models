function yd = OdeFunReconstruct(s,y,qei,Nf,L)

th = y(3);
b = BaseFcnLegendre(s,1,Nf);

pxd = cos(th);
pyd = sin(th);
thd = b'*qei;



yd = L*[pxd;pyd;thd];
end