function PlotRFRFR_Real(pos1,pos2,L,pend,geometry)
basepoint = geometry.basepoints;

% figure()


%% BEAM 1
plot(pos1(1,:),pos1(2,:),'k-','LineWidth',1.5)
hold on
plot(basepoint(1,1),basepoint(2,1),'ro','LineWidth',2)
line([basepoint(1,1),pos1(1,1)],[basepoint(2,1),pos1(2,1)],'Color','red','LineWidth',1.5)
line([pos1(1,end),pend(1)],[pos1(2,end),pend(2)],'Color','red','LineWidth',1.5)

%% BEAM 2
plot(pos2(1,:),pos2(2,:),'k-','LineWidth',1.5)
plot(basepoint(1,2),basepoint(2,2),'ro','LineWidth',2)
line([basepoint(1,2),pos2(1,1)],[basepoint(2,2),pos2(2,1)],'Color','red','LineWidth',1.5)
line([pos2(1,end),pend(1)],[pos2(2,end),pend(2)],'Color','red','LineWidth',1.5)

%% EE 
plot(pend(1),pend(2),'ro','LineWidth',2)

%% FRAME
quiver(0,0,L/5,0,'b','LineWidth',2)
quiver(0,0,0,L/5,'b','Linewidth',2)
axis equal
grid on
end