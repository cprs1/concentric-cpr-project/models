function [F1,F2] = getBaseEfforts(y,geometry,L,wd,Nf)
basepoints = geometry.basepoints;

Ci = [zeros(1,2);eye(2)];
qa = y(1:2,1);
qe = y(1+2:2+2*Nf,1);
lambda = y(1+2+2*Nf+2:2+2*Nf+2+2*2,1);

Fbase = zeros(6,1);
for i = 1:2
 % extract variables
    qai = qa(i);
    qei = qe(1+Nf*(i-1):Nf*i,1);
    lambdai = lambda(1+2*(i-1):2*i,1);
    p0 = basepoints(:,i);
    th0 = qai;
    
    %% FORWARD INTEGRATION: Geometry
    % initial value at s = 0
    y01 = [p0;th0;zeros(2+2*Nf,1);1;zeros(Nf,1)];
    
    % integration
    funforward = @(s,y) OdefunAssumedForward(s,y,qei,Nf,L,'fix');
    [~,y] = ode45(funforward,[0,1],y01);
    ygeom = y(end,:)';

    % extract results
    thL = ygeom(3,1);
    dthdqaL = ygeom(1+5+2*Nf,1); 
    dthdqeL = ygeom(1+5+2*Nf+1:end,1)';     
    
    % wrench on local - tip frame. 
    wrench = (Ci*lambdai);


    %% BACKWARD INTEGRATION: Loads
    % initial values at s = L
  
    y02 = [thL;wrench;zeros(Nf,1);
           dthdqaL;zeros(3+Nf,1);
           dthdqeL';zeros((3+Nf)*Nf,1);
          zeros(3,1);reshape(eye(3),3*3,1);zeros(3*Nf,1);
          1;zeros(3+Nf,1)];

            
    % integration
    funbackward = @(s,y) OdefunAssumeBackward(s,y,qei,wd,Nf,L,'fix');
    [~,y] = ode45(funbackward,[1,0],y02);
    yforces = y(end,:)';
    
    % extract results
    Fbase(1+3*(i-1):3*i,1) = -[yforces(2);Rz(qa(i))*yforces(3:4,1)]; % global frame base wrench
end

F1 = Fbase(1:3);
F2 = Fbase(4:6);

end