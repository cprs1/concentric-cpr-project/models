%% Robot Geometry Parameters
clear
close all
clc

%%
rAB = 0;
pA1 = [-rAB/2;0];
pA2 = [+rAB/2;0];
geometry.basepoints = [pA1,pA2];

b1 = 0.054;
b2 = 0.040;
L = .470;

geometry.rigidprox = b1;
geometry.rigidtip  = b2;

%% Robot Material Parameters

b = 0.002;
nb = 4;
A = 4*pi*b^2/4;
I = 4*pi*b^4/64;
E = 36.1*10^9;
rho = 1900;
EI = E*I;

stresslim = E*0.027; % Pa
torquelim = 2.5; %Nm

%% External loads

g = [0;-9.81];
fd = rho*A*g;
fend = [0;-0.218*9.81];


%% Model Parameters

Nf = 4;
Phi = @(s) BaseFcnLegendre(s,1,Nf)';
Kad = integral(@(s) Phi(s)'*EI*Phi(s),0,1,'ArrayValued',true);

th1lim = [0,2*pi];
th2lim = [0,2*pi];

pstart = [0;+0.4];
params.pstart = pstart;

%% Simulation functions
fcn.objetivefcn = @(y,pend) InverseModeForwBackRFRFSeqn_mex(y,geometry,Kad,L,fend,[0;fd],pend,Nf);
fcn.mechconstrfcn = @(y) mechconstrRFRFR(y,th1lim,th2lim,L,b/2,E,stresslim,torquelim,Nf,geometry,[0;fd]);
fcn.singufcn = @(jac,y) SingularityModeRFRFR(jac,y,Nf);
fcn.stabilityfcn = @(jac,y) StabilityModeRFRFR(jac,y,Nf);
fcn.internfcn = @(y) InternalEnergy(y,Kad,L,Nf,b/2,geometry,[0;fd]);
%% First Initial Guess

qa0 = [+pi/2;+pi/2];
qe0 = [.1;zeros(Nf-1,1);-.1;zeros(Nf-1,1)];
qp0 = pstart;
lambda0 = 0.0001*rand(4,1);
y0 = [qa0;qe0;qp0;lambda0];

feq = @(y) InverseModeForwBackRFRFSeqn(y,geometry,Kad,L,fend,[0;fd],pstart,Nf);

options = optimoptions('fsolve','display','iter','SpecifyObjectiveGradient',true,'CheckGradients',true,'Algorithm','trust-region');

[y,~,solve_flag,~,jac] = fsolve(feq,y0,options);
params.y0 = y;
[pos1,pos2] = posRFRFRmode(y,L,[pA1,pA2],geometry,Nf,100);

PlotRFRFR_Real(pos1,pos2,L,y(1+2+2*Nf:2+2*Nf+2,1),geometry);
title('Initial Guess Configuration')
drawnow
axis equal
axis(1.2*[-L L -L L])

drawnow