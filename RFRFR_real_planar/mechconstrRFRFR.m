function flag = mechconstrRFRFR(y,th1lim,th2lim,L,r,E,stresslim,torquelim,Nf,geometry,wd)

qA1 = wrapTo2Pi(y(1,1));
qA2 = wrapTo2Pi(y(2,1));
qe1 = y(1+2:2+Nf,1);
qe2 = y(1+2+Nf:2+2*Nf,1);

%% MOTOR CONSTRAINTS
flag1 = (th1lim(1)<=qA1 & qA1<=th1lim(2)) & (th2lim(1)<=qA2 & qA2<=th2lim(2));      % motor constraint 1


%% STRAIN CONSTRAINTS
Nsamp = 100; % sample in 100 points
s = linspace(0,L,Nsamp);
b = BaseFcnLegendre(s,L,Nf);

strain1 = b'*qe1;
strain2 = b'*qe2;
sigma1 = r*E*strain1;
sigma2 = r*E*strain2;
s1 = max(((sigma1).^2.).^0.5);
s2 = max(((sigma2).^2.).^0.5);
stress = max(s1,s2);
flag2 = stress<=stresslim;


%% MOTOR TORQUES
[F1,F2] = getBaseEfforts(y,geometry,L,wd,Nf);
torque1 = abs(F1(1));
torque2 = abs(F2(1));
torque = max(torque1,torque2);
flag3 = torque<=torquelim;

%% GLOBAL FLAG
flag = (flag1 & flag2 & flag3);

   
end