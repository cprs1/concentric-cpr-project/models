function yd = OdefunAssumedForward(s,y,qei,Nf,L,str)

th = y(3);
dthdqa = y(1+2+1+2+2*Nf,1); 
dthdqe = y(1+2+1+2+2*Nf+1:end,1); 
b = BaseFcnLegendre(s,1,Nf);

switch str
    case 'fix'
        pxd = cos(th);
        pyd = sin(th);
        thd = b'*qei;
        dpxdqad = -sin(th)*dthdqa;
        dpydqad = +cos(th)*dthdqa;
        dpxdqed = -sin(th)*dthdqe;
        dpydqed = +cos(th)*dthdqe;
        dthdqad = 0;
        dthdqed = b;
        yd = L*[pxd;pyd;thd;dpxdqad;dpydqad;dpxdqed;dpydqed;dthdqad;dthdqed];
    case 'variable'
        pxd = L*cos(th);
        pyd = L*sin(th);
        thd = L*b'*qei;
        dpxdqad = -L*sin(th)*dthdqa + cos(th);
        dpydqad = +L*cos(th)*dthdqa + sin(th);
        dpxdqed = L*-sin(th)*dthdqe;
        dpydqed = L*+cos(th)*dthdqe;
        dthdqad = b'*qei;
        dthdqed = L*b;
        yd = [pxd;pyd;thd;dpxdqad;dpydqad;dpxdqed;dpydqed;dthdqad;dthdqed];
    otherwise
        error('Define settings for beams length');
end

end