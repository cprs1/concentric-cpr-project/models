%% Robot Geometry Parameters
rAB = 0;
pA1 = [-rAB/2;0];
pA2 = [+rAB/2;0];
geometry.basepoints = [pA1,pA2];

%% Robot Material Parameters
L = 0.560;
d = 0.002;
nb = 4;
A = 4*pi*d^2/4;
I = 4*pi*d^4/64;
E = 36.1*10^9;
rho = 1900;
EI = E*I;

stresslim = 0.0275*E; % Pa
difflim = Inf;

%% External loads

g = [0;0];
fd = rho*A*g;
fend = [0;-0.281*9.81];


%% Model Parameters

Nf = 4;
Phi = @(s) BaseFcnLegendre(s,1,Nf)';
Kad = integral(@(s) Phi(s)'*EI*Phi(s),0,1,'ArrayValued',true);

th1lim = [0,2*pi];
th2lim = [0,2*pi];

pstart = [0;0.4];
params.pstart = pstart;

%% Simulation functions
fcn.objetivefcn = @(y,pend) InverseModeForwBackRFRFSeqn_mex(y,geometry,Kad,L,fend,fd,pend,Nf);
fcn.mechconstrfcn = @(y) mechconstrRFRFR(y,th1lim,th2lim,L,b/2,E,stresslim,difflim,Nf,pA1,pA2);
fcn.singufcn = @(jac,y) SingularityModeRFRFR(jac,y,Nf);
fcn.stabilityfcn = @(jac,y) StabilityModeRFRFR(jac,y,Nf);
fcn.internfcn = @(y) InternalEnergy(y,Kad,L,Nf);
%% First Initial Guess

qa0 = [+pi;0];
qe0 = 0.001*zeros(2*Nf,1);
qp0 = pstart;
lambda0 = zeros(4,1);
y0 = [qa0;qe0;qp0;lambda0];

feq = @(y) InverseModeForwBackRFRFSeqn(y,geometry,Kad,L,fend,fd,pstart,Nf);

options = optimoptions('fsolve','display','iter','SpecifyObjectiveGradient',true,'CheckGradients',true);

[y,~,solve_flag,~,jac] = fsolve(feq,y0,options);
params.y0 = y;
[pos1,pos2] = posRFRFRmode(y,L,[pA1,pA2],Nf,100);

PlotRFRFR(pos1,pos2,L);
title('Initial Guess Configuration')
drawnow
axis equal
axis([-.6 .6 -.6 .6])

drawnow