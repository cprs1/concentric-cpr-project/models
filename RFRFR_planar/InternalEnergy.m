function energy = InternalEnergy(y,Kad,L,Nf)
qe = y(1+2:2+2*Nf,1);
qe1 = qe(1+Nf*(1-1):Nf*1,1);
qe2 = qe(1+Nf*(2-1):Nf*2,1);
energy = 0.5*L*qe1'*Kad*qe1 + 0.5*L*qe2'*Kad*qe2;
end