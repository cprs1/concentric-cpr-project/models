function flag = mechconstrRFRFR(y,th1lim,th2lim,L,r,E,stresslim,difflim,Nf,pA1,pA2)

qA1 = wrapTo2Pi(y(1,1));
qA2 = wrapTo2Pi(y(2,1));
qe1 = y(1+2:2+Nf,1);
qe2 = y(1+2+Nf:2+2*Nf,1);

%% MOTOR CONSTRAINTS
flag1 = (th1lim(1)<=qA1 & qA1<=th1lim(2));      % motor constraint 1
flag2 = (th2lim(1)<=qA2 & qA2<=th2lim(2));  	% motor constraint 2

%% STRAIN CONSTRAINTS
Nsamp = 100; % sample in 100 points
s = linspace(0,L,Nsamp);
b = BaseFcnLegendre(s,L,Nf);

strain1 = b'*qe1;
strain2 = b'*qe2;
sigma1 = r*E*strain1;
sigma2 = r*E*strain2;
s1 = max(((sigma1).^2.).^0.5);
s2 = max(((sigma2).^2.).^0.5);
stress = max(s1,s2);
flag4 = stress<=stresslim;

%% BEAMS' INTERFERENCE CONSTRAINT
[pos1,pos2] = posRFRFRmode(y,L,[pA1,pA2],Nf,Nsamp);
scale_points = 15;
Mx1 = repmat(pos1(1,1:end-scale_points),Nsamp-scale_points,1);
My1 = repmat(pos1(2,1:end-scale_points),Nsamp-scale_points,1);

Mx2 = repmat(pos2(1,1:end-scale_points)',1,Nsamp-scale_points);
My2 = repmat(pos2(2,1:end-scale_points)',1,Nsamp-scale_points);

diffx = abs(Mx1-Mx2);
diffy = abs(My1-My2);

[row,col] = find(diffx<difflim & diffy<difflim);

if numel(row)>0
    flag5 = 0;
else
    flag5 = 1;
end

%% GLOBAL FLAG
flag = (flag1 & flag2 & flag4 & flag5);

   
end