%% SINGUTEST

clear
close all
clc

%%
rAB = 0;
pA1 = [-rAB/2;0];
pA2 = [+rAB/2;0];
geometry.basepoints = [pA1,pA2];

%% Robot Material Parameters
L = 0.560;
d = 0.002;
nb = 4;
A = 4*pi*d^2/4;
I = 4*pi*d^4/64;
E = 36.1*10^9;
rho = 1900;
EI = E*I;

stresslim = 0.0275*E; % Pa
difflim = Inf;

%% External loads

g = [0;0];
fd = rho*A*g;
fend = [0;-0.281*9.81];


%% Model Parameters

Nf = 4;
Phi = @(s) BaseFcnLegendre(s,1,Nf)';
Kad = integral(@(s) Phi(s)'*EI*Phi(s),0,1,'ArrayValued',true);

qpd = [0;0.4];

qa0 = [+pi;0];
qe0 = 0.001*zeros(2*Nf,1);
qp0 = qpd;
lambda0 = 0.001*rand(4,1);
y0 = [qa0;qe0;qp0;lambda0];

feq = @(y) InverseModeForwBackRFRFSeqn(y,geometry,Kad,L,fend,fd,qpd,Nf);
options = optimoptions('fsolve','display','iter','SpecifyObjectiveGradient',true,'CheckGradients',true);

[sol,~,solve_flag,~,jac] = fsolve(feq,y0,options);

[pos1,pos2] = posRFRFRmode(sol,L,[pA1,pA2],Nf,100);

PlotRFRFR(pos1,pos2,L);

%% MOVE
options = optimoptions('fsolve','display','off','Algorithm','trust-region','Maxiter',10,'SpecifyObjectiveGradient',true,'CheckGradients',false);

i = 1;
flag = 1;
i_max = 1000;
while i<i_max && flag>0
    guess0 = sol;
    qpd = qpd-[0;0.002];
    fun = @(y) InverseModeForwBackRFRFSeqn(y,geometry,Kad,L,fend,fd,qpd,Nf);
    [sol,~,flag,~,jac] = fsolve(fun,guess0,options);
    jac = full(jac);
    yy(i) = qpd(2);
    flags(i) = StabilityModeRFRFR(jac,sol,Nf);
    [t1,t2] = SingularityModeRFRFR(jac,sol,Nf);
    flagt1(i) = t1;
    flagt2(i) = t2;
    i = i+1
end

[pos1,pos2] = posRFRFRmode(sol,L,[pA1,pA2],Nf,100);
hold on
PlotRFRFR(pos1,pos2,L);

%% RESULTS
figure()
subplot(1,2,1)
yyaxis left
plot(yy,flags)
yyaxis right
semilogy(yy,flagt2)
grid on
subplot(1,2,2)
semilogy(yy,flagt1)
grid on